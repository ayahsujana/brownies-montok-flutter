class TotalTransaction {
  final String id;
  final String idMenu;
  final String menuName;
  final String quantity;
  final String price;
  final String weight;
  final String tmpImage;
  final String idCustomer;
  final String username;
  final String total;

  TotalTransaction(
      this.id,
      this.idMenu,
      this.menuName,
      this.quantity,
      this.price,
      this.weight,
      this.tmpImage,
      this.idCustomer,
      this.username,
      this.total);
}

class TotalOrders {
  final String total;

  TotalOrders(this.total);
}
