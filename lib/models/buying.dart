class Pembeli {
  final String id;
  final String nama;
  final String alamat;
  final String kota;
  final String provinsi;
  final String kurir;
  final String kapanKirim;
  final String telpon;
  final String pesanan;
  final String status;
  final String komen;
  final String email;

  Pembeli(
      this.id,
      this.nama,
      this.alamat,
      this.kota,
      this.provinsi,
      this.kurir,
      this.kapanKirim,
      this.telpon,
      this.pesanan,
      this.status,
      this.komen,
      this.email);
}
