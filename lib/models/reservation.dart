class Reservation{
  final String id;
  final String name;
  final String alamat;
  final String kota;
  final String provinsi;
  final String kurir;
  final String waktuKirim;
  final String telpon;
  final String pesanan;
  final String status;
  final String pesan;
  final String email;

  Reservation(this.id, this.name, this.alamat, this.kota, this.provinsi, this.kurir, this.waktuKirim, this.telpon, this.pesanan, this.status, this.pesan, this.email);
}