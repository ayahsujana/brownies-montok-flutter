import 'dart:async';
import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:ketixclone/api.dart';
import 'package:ketixclone/view/dashboard.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  _LoginPageState createState() => _LoginPageState();
}

enum FormType { login, register }
enum AuthStatus { masuk, keluar }

class _LoginPageState extends State<LoginPage> {
  String _email, _password, _nama;
  FormType _formType = FormType.login;
  AuthStatus _authStatus = AuthStatus.keluar;
  final _formKey = new GlobalKey<FormState>();
  var isLoading;
  bool _obscureText = true;

  _validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      if (_formType == FormType.login) {
        _formLogin();
      } else {
        _formRegister();
      }

      isLoading == true ? _showToast() : SizedBox();
    }
  }

  _formLogin() async {
    setState(() {
      isLoading = true;
    });
    final responses = await http.post(ApiUrl.login, body: {
      'username': _email.trim(),
      'password': _password.trim(),
    });

    final data = json.decode(responses.body);
    int val = data["value"];
    String msg = data["message"];
    if (val == 1) {
      print(responses.body);
      setState(() {
        _savePreferences(data['idCustomer'], data['username'], data['level'],
            data['nama'], data['createdDate']);
        _authStatus = AuthStatus.masuk;
        Navigator.pop(context);
      });
    } else if (val == 0) {
      print(responses.body);
      setState(() {
        isLoading = false;
      });
      _warningDialog(msg);
    }
  }

  _formRegister() async {
    setState(() {
      isLoading = true;
    });

    final responses = await http.post(ApiUrl.register, body: {
      'username': _email.trim(),
      'password': _password.trim(),
      'nama': _nama.trim(),
    });

    final data = json.decode(responses.body);
    int val = data["value"];
    String msg = data["message"];
    if (val == 1) {
      _warningDialog(msg);
      setState(() {
        isLoading = false;
      });
    } else if (val == 0) {
      _warningDialog(msg);
      setState(() {
        isLoading = false;
      });
    } else {
      _warningDialog(msg);
      setState(() {
        isLoading = false;
      });
    }
  }

  void _moveToRegister() {
    _formKey.currentState.reset();
    setState(() {
      _formType = FormType.register;
    });
  }

  void _moveToLogin() {
    _formKey.currentState.reset();
    setState(() {
      _formType = FormType.login;
    });
  }

  _savePreferences(String idCustomer, String username, String level,
      String nama, String createdDate) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setString('idCustomer', idCustomer);
      preferences.setString('username', username);
      preferences.setString('level', level);
      preferences.setString('nama', nama);
      preferences.setString('createdDate', createdDate);
    });
  }

  String level, username;
  _getPreference() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      level = preferences.getString('level');
      username = preferences.getString('username');

      _authStatus = level == "1"
          ? AuthStatus.masuk
          : level == "2" ? AuthStatus.masuk : AuthStatus.keluar;
    });
  }

  _keluar() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setString('level', null);
      preferences.setString('idCustomer', null);
      preferences.setString('username', null);
      preferences.commit();
    });

    _authStatus = AuthStatus.keluar;
  }

  @override
  void initState() {
    super.initState();
    _getPreference();
    _checkKoneksi();
  }

  @override
  Widget build(BuildContext context) {
    switch (_authStatus) {
      case AuthStatus.keluar:
        return Scaffold(
          body: _formField(),
        );
        break;
      case AuthStatus.masuk:
        return DashboardApp(_keluar, _statusKoneksi);
        break;
    }
  }

  bool _statusKoneksi = false;
  _checkKoneksi() async {
    var connectivityResult = await new Connectivity().checkConnectivity();
    _statusKoneksi = connectivityResult == ConnectivityResult.none;
  }

  Widget _formField() {
    if (_formType == FormType.login) {
      return Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.only(left: 16.0, right: 16.0),
          child: Center(
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                //Image.asset('./img/brownies.png'),
                ListTile(
                  leading: Icon(Icons.email, color: Colors.brown),
                  title: TextFormField(
                    decoration: InputDecoration(
                        labelText: "Email Address",
                        labelStyle: TextStyle(
                          color: Colors.brown,
                        )),
                    keyboardType: TextInputType.emailAddress,
                    maxLines: 1,
                    validator: (value) {
                      if (value.contains('@')) {
                        return null;
                      } else {
                        return 'Mohon masukkan email dengan benar';
                      }
                    },
                    onSaved: (value) => _email = value,
                  ),
                ),
                ListTile(
                  leading: Icon(Icons.lock, color: Colors.brown),
                  title: TextFormField(
                    decoration: InputDecoration(
                      labelText: "Password",
                      labelStyle: TextStyle(
                        color: Colors.brown,
                      ),
                      suffixIcon: GestureDetector(
                        onTap: () {
                          setState(() {
                            _obscureText = !_obscureText;
                          });
                        },
                        child: Icon(
                          _obscureText
                              ? Icons.visibility
                              : Icons.visibility_off,
                        ),
                      ),
                    ),
                    keyboardType: TextInputType.text,
                    maxLines: 1,
                    maxLength: 10,
                    obscureText: _obscureText,
                    validator: (value) =>
                        value.isEmpty ? "Password wajib diisi" : null,
                    onSaved: (value) => _password = value,
                  ),
                ),
                RaisedButton(
                  child: Text("Login", style: TextStyle(color: Colors.white)),
                  color: Colors.brown,
                  onPressed: _validateAndSave,
                ),
                FlatButton(
                  child: Text(
                    "Belum memiliki akun? Klik disini.",
                    style: TextStyle(color: Colors.brown),
                  ),
                  onPressed: _moveToRegister,
                )
              ],
            ),
          ),
        ),
      );
    } else {
      return Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.only(left: 16.0, right: 16.0),
          child: Center(
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                //Image.asset('./img/brownies.png'),
                ListTile(
                  leading: Icon(Icons.email, color: Colors.brown),
                  title: TextFormField(
                    decoration: InputDecoration(
                        labelText: "Nama Lengkap",
                        labelStyle: TextStyle(
                          color: Colors.brown,
                        )),
                    keyboardType: TextInputType.text,
                    maxLines: 1,
                    validator: (value) =>
                        value.isEmpty ? "Nama wajib diisi" : null,
                    onSaved: (value) => _nama = value,
                  ),
                ),
                ListTile(
                  leading: Icon(Icons.email, color: Colors.brown),
                  title: TextFormField(
                    decoration: InputDecoration(
                        labelText: "Email Address",
                        labelStyle: TextStyle(
                          color: Colors.brown,
                        )),
                    keyboardType: TextInputType.emailAddress,
                    maxLines: 1,
                    validator: (value) {
                      if (value.contains('@')) {
                        return null;
                      } else {
                        return 'Mohon masukkan email dengan benar';
                      }
                    },
                    onSaved: (value) => _email = value,
                  ),
                ),
                ListTile(
                  leading: Icon(Icons.lock, color: Colors.brown),
                  title: TextFormField(
                    decoration: InputDecoration(
                      labelText: "Password",
                      labelStyle: TextStyle(
                        color: Colors.brown,
                      ),
                      suffixIcon: GestureDetector(
                        onTap: () {
                          setState(() {
                            _obscureText = !_obscureText;
                          });
                        },
                        child: Icon(
                          _obscureText
                              ? Icons.visibility
                              : Icons.visibility_off,
                        ),
                      ),
                    ),
                    keyboardType: TextInputType.text,
                    maxLines: 1,
                    maxLength: 10,
                    obscureText: _obscureText,
                    validator: (value) =>
                        value.isEmpty ? "Password wajib diisi" : null,
                    onSaved: (value) => _password = value,
                  ),
                ),
                RaisedButton(
                  child: Text(
                    "SignUp",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.brown,
                  onPressed: _validateAndSave,
                ),
                FlatButton(
                  child: Text(
                    "Have an account? Login",
                    style: TextStyle(color: Colors.brown),
                  ),
                  onPressed: _moveToLogin,
                )
              ],
            ),
          ),
        ),
      );
    }
  }

  _showToast() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(8.0),
                  color: Colors.black,
                  child: Text(
                    'Please wait...',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                    padding: EdgeInsets.all(8.0),
                    child: Center(
                      child: CircularProgressIndicator(),
                    )),
              ],
            ),
          );
        });
  }

  Future<bool> _warningDialog(String msg) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(16.0),
                      color: Colors.black,
                      child: Text(
                        'Congratulation!',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(16.0),
                      child: Text('$msg'),
                    ),
                    Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: SizedBox(),
                          ),
                          FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop(true);
                              setState(() {
                                Navigator.of(context).pop(true);
                                _moveToLogin();
                              });
                            },
                            child: Text('OK'),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ) ??
              false;
        });
  }
}
