import 'package:flutter/material.dart';
import 'package:ketixclone/view/everything/informasi.dart';
import 'package:ketixclone/view/login/login.dart';

class SuksesCheckout extends StatefulWidget {
  _SuksesCheckoutState createState() => _SuksesCheckoutState();
}

class _SuksesCheckoutState extends State<SuksesCheckout> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          'Order Confirmation',
          style: TextStyle(color: Colors.black),
        ),
        leading: FlatButton(
          child: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => LoginPage()),
                (Route<dynamic> route) => false);
          },
        ),
      ),
      body: WillPopScope(
        onWillPop: () {},
        child: Container(
          child: Center(
            child: ListView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Image.asset(
                    './img/success.png',
                    height: 150.0,
                    width: 250.0,
                  ),
                ),
                SizedBox(
                  height: 50.0,
                ),
                Center(
                    child: Text(
                  'Pesanan Berhasil Terkirim',
                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                )),
                SizedBox(
                  height: 5.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Silahkan lakukan pembayaran ',
                    ),
                    GestureDetector(
                      child: Text(
                        'info lebih lanjut!',
                        style: TextStyle(color: Colors.blue),
                      ),
                      onTap: () {
                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                                builder: (context) => BagianInformasi()),
                            (Route<dynamic> route) => false);
                      },
                    ),
                  ],
                ),
                SizedBox(
                  height: 50.0,
                ),
                // Padding(
                //   padding: const EdgeInsets.only(
                //       left: 20.0, right: 20.0, top: 20.0, bottom: 5.0),
                //   child: OutlineButton(
                //     onPressed: () {
                //       Navigator.of(context).push(MaterialPageRoute(
                //           builder: (context) => TransactionStatus()));
                //     },
                //     color: Colors.green,
                //     highlightColor: Colors.green,
                //     splashColor: Colors.green,
                //     padding: EdgeInsets.all(16.0),
                //     borderSide: BorderSide(
                //         style: BorderStyle.solid, color: Colors.green),
                //     child: Text('Status Pemesanan'),
                //   ),
                // ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 20.0, right: 20.0, top: 5.0),
                  child: OutlineButton(
                    onPressed: () {
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(builder: (context) => LoginPage()),
                          (Route<dynamic> route) => false);
                    },
                    color: Colors.green,
                    highlightColor: Colors.green,
                    splashColor: Colors.green,
                    padding: EdgeInsets.all(16.0),
                    borderSide: BorderSide(
                        style: BorderStyle.solid, color: Colors.green),
                    child: Text('Kembali ke Menu'),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
