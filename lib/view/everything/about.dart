import 'package:flutter/material.dart';

class AboutUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        scrollDirection: Axis.vertical,
        slivers: <Widget>[
          SliverAppBar(
            expandedHeight: 200.0,
            pinned: true,
            backgroundColor: Colors.white,
            elevation: 0.0,
            flexibleSpace: FlexibleSpaceBar(
              //title: Text('About Us'),
              background:
                  Image.asset('./img/brownies_header.jpg', fit: BoxFit.cover),
            ),
          ),
          SliverToBoxAdapter(
              child: Card(
            elevation: 2.0,
            child: Container(
              child: Row(
                children: <Widget>[
                  Flexible(
                    flex: 3,
                    child: Image.asset('./img/brownies_icon.jpg'),
                  ),
                  Flexible(
                    flex: 4,
                    child: Center(
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Image.asset('./img/brownies_logo.png'),
                      ),
                    ),
                  )
                ],
              ),
            ),
          )),
          SliverToBoxAdapter(
              child: Card(
            elevation: 2.0,
            child: Container(
              child: Padding(
                padding: EdgeInsets.all(16.0),
                child: Text(
                  
                    '   Brownies Montok Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.\n\n   Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.\n\n   Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.\n\n   Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.',
                    textAlign: TextAlign.justify,),
              ),
            ),
          )),
        ],
      ),
    );
  }
}
