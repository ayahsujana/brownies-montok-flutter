import 'package:flutter/material.dart';
import 'package:ketixclone/customWidget/widget.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:ketixclone/view/cart/allCart.dart';
import 'package:ketixclone/view/everything/about.dart';
import 'package:ketixclone/view/everything/informasi.dart';
import 'package:ketixclone/view/product/allCategory.dart';
import 'package:ketixclone/view/promotion/promo.dart';
import 'package:ketixclone/view/transaction/homeTransaction.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String nama;
  _getPreference() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      nama = preferences.getString('nama');
    });
    print('$nama');
  }

  @override
  void initState() {
    super.initState();
    _getPreference();
  }

  @override
  Widget build(BuildContext context) {
    var imageSlide = new SizedBox(
        height: 150.0,
        width: double.infinity,
        child: new Carousel(
          images: [
            new AssetImage("./img/banner1.png"),
            new AssetImage("./img/banner2.png"),
            new AssetImage("./img/banner3.png"),
            new AssetImage("./img/banner4.png"),
          ],
          showIndicator: false,
          boxFit: BoxFit.fill,
          autoplayDuration: Duration(milliseconds: 5000),
          indicatorBgPadding: 1.0,
        ));
    return ListView(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      children: <Widget>[
        imageSlide,
        Padding(
          padding: EdgeInsets.all(8.0),
          child: Row(children: <Widget>[
            Text('Hi '),
            Text(
              '$nama',
              style: TextStyle(
                color: Colors.brown,
              ),
            ),
            Text(', mau belanja apa hari ini?'),
          ]),
        ),
        GridView.count(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          crossAxisCount: 3,
          childAspectRatio: 1.3,
          mainAxisSpacing: 8.0,
          crossAxisSpacing: 8.0,
          padding: EdgeInsets.only(left:8.0, right: 8.0),
          children: <Widget>[
            ListItem(
              img: "./img/product.png",
              text: "Product",
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => CategoryProduct()));
              },
            ),
            ListItem(
              img: "./img/cart.png",
              text: "Cart",
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AllCartProduct()));
              },
            ),
            ListItem(
                img: "./img/transactions.png",
                text: "Transactions",
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => TransactionStatus()));
                }),
            ListItem(
              img: "./img/specialdeals.png",
              text: "Promo",
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PromoMenu()));
              },
            ),
            ListItem(
              img: "./img/help.png",
              text: "Information",
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => BagianInformasi()));
              },
            ),
            ListItem(
              img: "./img/about_us.png",
              text: "About Us",
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AboutUs()));
              },
            ),
          ],
        ),
      ],
    );
  }
}
