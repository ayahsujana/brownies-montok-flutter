import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:ketixclone/api.dart';
import 'package:ketixclone/models/transaksi.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TransactionStatus extends StatefulWidget {
  _TransactionStatusState createState() => _TransactionStatusState();
}

class _TransactionStatusState extends State<TransactionStatus> {
  final transaksi = List<TransactionModels>();
  String username, idCustomer;
  bool isLoading = false;
  bool cek = false;

  _getPreference() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      username = preferences.getString('username');
      idCustomer = preferences.getString('idCustomer');
    });
    _fetchDataTransaksi();
  }

  Future<Null> _fetchDataTransaksi() async {
    transaksi.clear();
    setState(() {
      isLoading = true;
    });

    final responses =
        await http.get(ApiUrl.baseUrl + ApiUrl.invTransaksi + idCustomer);
    if (responses.contentLength == 2) {
      setState(() {
        isLoading = false;
        cek = false;
      });
    } else {
      print('${responses.body}');
      final getData = jsonDecode(responses.body);
      getData.forEach((api) {
        final trx = new TransactionModels(
            api['id'],
            api['invoice'],
            api['tglTransaksi'],
            api['status'],
            api['idCustomer'],
            api['username'],
            api['nama']);
        transaksi.add(trx);
      });
      setState(() {
        isLoading = false;
        cek = true;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _getPreference();
  }

  @override
  Widget build(BuildContext context) {
    var cekTransaksiEmpty = isLoading
        ? Center(
            child: CircularProgressIndicator(),
          )
        : Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                './img/no_order.png',
                height: 150.0,
                width: 100.0,
              ),
              SizedBox(
                height: 16.0,
              ),
              Text(
                'Anda belum memiliki transaksi',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20.0),
              )
            ],
          );
    var cekTransaksiFound = isLoading
        ? Center(
            child: CircularProgressIndicator(),
          )
        : ListView.builder(
            itemCount: transaksi.length,
            itemBuilder: (context, index) {
              final bukti = transaksi[index];
              return GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DetailTransaksi(bukti)));
                },
                child: Card(
                  elevation: 2.0,
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        ListTile(
                          leading: CircleAvatar(
                            maxRadius: 30.0,
                            backgroundImage: AssetImage(
                              './img/brownies_icon.jpg',
                            ),
                          ),
                          title: Text(
                            'INV/201812/XVIII/' + bukti.invoice,
                            style: TextStyle(
                                color: Colors.green,
                                fontWeight: FontWeight.bold),
                          ),
                          subtitle: Text(bukti.tglTransaksi),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Text(
                          bukti.status == '0'
                              ? 'Menunggu Pembayaran...'
                              : bukti.status == '1'
                                  ? 'Dibayarkan...'
                                  : bukti.status == '2'
                                      ? 'Sedang Dikirim...'
                                      : 'Transaksi Selesai...',
                          style: TextStyle(fontStyle: FontStyle.italic),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          );
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text('Status Transaksi', style: TextStyle(color: Colors.black)),
        leading: FlatButton(
          child: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.pop(context);
            // Navigator.of(context).pushAndRemoveUntil(
            //     MaterialPageRoute(builder: (context) => LoginPage()),
            //     (Route<dynamic> route) => false);
          },
        ),
      ),
      body: Container(
        child: RefreshIndicator(
          onRefresh: _fetchDataTransaksi,
          child: cek ? cekTransaksiFound : cekTransaksiEmpty,
        ),
      ),
    );
  }
}

class DetailTransaksi extends StatefulWidget {
  final TransactionModels models;
  DetailTransaksi(this.models);
  _DetailTransaksiState createState() => _DetailTransaksiState();
}

class _DetailTransaksiState extends State<DetailTransaksi> {
  final dtransaksi = List<DetailProdukTransaksiModels>();
  final oCcy = new NumberFormat("#,##0", "en_US");
  var isLoading = false;
  var cek = false;

  _fetchDataDetailTransaksi() async {
    dtransaksi.clear();
    setState(() {
      isLoading = true;
    });

    final responses = await http
        .get(ApiUrl.baseUrl + ApiUrl.detilTransaksi + widget.models.id);
    final data = jsonDecode(responses.body);
    print('$data');
    data.forEach((api) {
      final dTrx = new DetailProdukTransaksiModels(
        api['id'],
        api['idTransaksi'],
        api['Menu_ID'],
        api['Quantity'],
        api['Price'],
        api['Menu_name'],
        api['Menu_image'],
        api['Weight'],
        api['total'],
      );
      dtransaksi.add(dTrx);
    });
    setState(() {
      isLoading = false;
      cek = true;
    });
  }

  final sumTotal = List<SumDetailTotalTransaksi>();
  Future<void> _getTotalTransaksi() async {
    sumTotal.clear();
    setState(() {
      isLoading = true;
    });

    final responses = await http
        .get(ApiUrl.baseUrl + ApiUrl.sumDetailTransaksi + widget.models.id);
    final data = jsonDecode(responses.body);
    data.forEach((api) {
      final dTot = new SumDetailTotalTransaksi(api['total']);
      sumTotal.add(dTot);
    });
    setState(() {
      isLoading = false;
      cek = true;
    });
  }

  // _backToMenu() {
  //   Navigator.of(context).pushAndRemoveUntil(
  //       MaterialPageRoute(builder: (context) => LoginPage()),
  //       (Route<dynamic> route) => false);
  // }

  @override
  void initState() {
    super.initState();
    _fetchDataDetailTransaksi();
    _getTotalTransaksi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          'Detil Transaksi',
          style: TextStyle(color: Colors.black),
        ),
        leading: FlatButton(
          child: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  cek
                      ? ListView.builder(
                          padding: EdgeInsets.all(8.0),
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: dtransaksi.length,
                          itemBuilder: (context, index) {
                            final sp = dtransaksi[index];
                            return ListView(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 8.0, top: 8.0),
                                  child: Text(
                                    sp.menuName,
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Divider(),
                                Row(
                                  children: <Widget>[
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(left: 8.0),
                                      child: Container(
                                        child: Image.network(
                                          ApiUrl.baseUrl + sp.menuImage,
                                          height: 75.0,
                                          width: 100.0,
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    ),
                                    VerticalDivider(),
                                    Container(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Column(
                                                children: <Widget>[
                                                  Text(
                                                    'Qty',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  SizedBox(
                                                    height: 5.0,
                                                  ),
                                                  Text(sp.quantity)
                                                ],
                                              ),
                                              VerticalDivider(
                                                color: Colors.black,
                                              ),
                                              Column(
                                                children: <Widget>[
                                                  Text(
                                                    'Berat',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  SizedBox(
                                                    height: 5.0,
                                                  ),
                                                  Text(sp.menuWeight + ' Kg')
                                                ],
                                              ),
                                              VerticalDivider(
                                                color: Colors.black,
                                              ),
                                              Column(
                                                children: <Widget>[
                                                  Text(
                                                    'Harga Satuan',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  SizedBox(
                                                    height: 5.0,
                                                  ),
                                                  Text('Rp. ' +
                                                      oCcy.format(int.parse(
                                                          sp.price)) +
                                                      '.-')
                                                ],
                                              )
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                Divider(),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 8.0, right: 8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text('Sub Total'),
                                      Text(
                                        'Rp. ' +
                                            oCcy.format(int.parse(sp.total)) +
                                            '.-',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ),
                                Divider(),
                              ],
                            );
                          },
                        )
                      : _noAddtoCart(),
                  cek
                      ? ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: sumTotal.length,
                          itemBuilder: (BuildContext context, int index) {
                            final to = sumTotal[index];
                            return ListView(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              padding:
                                  EdgeInsets.only(left: 10.0, right: 10.0),
                              children: <Widget>[
                                SizedBox(
                                  height: 16.0,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Jumlah',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      'Rp. ' +
                                          oCcy.format(int.parse(to.total)) +
                                          '.-',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 5.0,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Ongkos kirim',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      'Free',
                                      style: TextStyle(
                                          color: Colors.green,
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 5.0,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'PPN 0%',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      'Rp. 0,-',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 16.0,
                                ),
                                SizedBox(
                                  height: 16.0,
                                )
                              ],
                            );
                          },
                        )
                      : SizedBox(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _noAddtoCart() {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Center(
        child: CircularProgressIndicator(
          strokeWidth: 2.0,
        ),
      ),
    );
  }
}
