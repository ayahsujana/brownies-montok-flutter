import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:ketixclone/api.dart';
import 'package:ketixclone/models/menu.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:ketixclone/view/product/detailsProduct.dart';

class NewsUpdate extends StatefulWidget {
  _NewsUpdateState createState() => _NewsUpdateState();
}

class _NewsUpdateState extends State<NewsUpdate> {
  final oCcy = new NumberFormat("#,##0", "en_US");
  final menuTerbaru = List<MenuItems>();
  bool isLoading, isCheck;

  _fetchMenuTerbaru() async {
    menuTerbaru.clear();
    setState(() {
      isLoading = true;
    });

    final responses =
        await http.get(ApiUrl.baseUrl + ApiUrl.hotnews + ApiUrl.accessKey);

    if (responses.contentLength == 2) {
      setState(() {
        isLoading = false;
        isCheck = false;
      });
    } else {
      final getData = jsonDecode(responses.body);
      getData.forEach((api) {
        final hot = new MenuItems(
            api['Menu_ID'],
            api['Menu_name'],
            api['Price'],
            api['Weight'],
            api['Serve_for'],
            api['Menu_image'],
            api['Description'],
            api['Quantity']);
        menuTerbaru.add(hot);
      });
      setState(() {
        isLoading = false;
        isCheck = true;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _fetchMenuTerbaru();
  }

  @override
  Widget build(BuildContext context) {
    return _buildNewItems();
  }

  Widget _buildNewItems() {
    if (isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return Container(
        height: MediaQuery.of(context).size.height,
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: menuTerbaru.length,
          itemBuilder: (context, i) {
            final b = menuTerbaru[i];
            return GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ProductDetails(b)));
              },
              child: Card(
                elevation: 5.0,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child: Padding(
                          padding: const EdgeInsets.all(0.0),
                          child: Hero(
                            tag: b.idMenu,
                            child: FadeInImage.assetNetwork(
                              placeholder: './img/loading_icon.gif',
                              image: ApiUrl.baseUrl + b.imageMenu,
                              fit: BoxFit.fill,
                              height: 100.0,
                            ),
                          )),
                    ),
                    Expanded(
                      flex: 3,
                      child: Container(
                        padding: EdgeInsets.only(left: 8.0, top: 6.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              b.nameMenu,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                            Text(
                              b.description,
                              maxLines: 2,
                              style: TextStyle(
                                fontSize: 10.0,
                              ),
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            Text(
                              'Rp. ' + oCcy.format(int.parse(b.price)) + '.-',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      );
    }
  }
}
