import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:ketixclone/api.dart';
import 'package:ketixclone/models/category.dart';
import 'package:ketixclone/models/menu.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:ketixclone/view/cart/allCart.dart';
import 'package:ketixclone/view/product/detailsProduct.dart';

class ProductByCategory extends StatefulWidget {
  final CategoryItems categoryId;
  ProductByCategory(this.categoryId);
  _ProductByCategoryState createState() => _ProductByCategoryState();
}

class _ProductByCategoryState extends State<ProductByCategory> {
  final oCcy = new NumberFormat("#,##0", "en_US");
  final menu = List<MenuItems>();
  bool isLoading = false;
  bool isCheck = false;

  _fetchMenuItems() async {
    menu.clear();
    setState(() {
      isLoading = true;
    });
    final responses = await http
        .get(ApiUrl.baseUrl + ApiUrl.menuItem + widget.categoryId.idCategory);

    if (responses.contentLength == 2) {
      setState(() {
        isLoading = false;
        isCheck = false;
      });
    } else {
      final getData = jsonDecode(responses.body);
      //print('$getData');
      getData.forEach((api) {
        final mn = new MenuItems(
          api['Menu_ID'],
          api['Menu_name'],
          api['Price'],
          api['Weight'],
          api['Serve_for'],
          api['Menu_image'],
          api['Description'],
          api['Quantity'],
        );
        menu.add(mn);
      });
      setState(() {
        isLoading = false;
        isCheck = true;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _fetchMenuItems();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          widget.categoryId.nameCategory,
          style: TextStyle(color: Colors.black),
        ),
        leading: FlatButton(
          child: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => AllCartProduct()));
            },
            child: Padding(
              padding: const EdgeInsets.all(14.0),
              child: Image.asset(
                './img/toko_buku.png',
                height: 30.0,
                width: 30.0,
              ),
            ),
          )
        ],
      ),
      body: isCheck ? _buildMenuItems() : _buildNullItems(),
    );
  }

  Widget _buildNullItems() {
    if (isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return Center(
        child: Text('Sorry, No Items Available. Back Again Later'),
      );
    }
  }

  Widget _buildMenuItems() {
    if (isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return Container(
        child: ListView.builder(
          itemCount: menu.length,
          itemBuilder: (context, index) {
            final catMenu = menu[index];
            return Container(
              width: double.infinity,
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ProductDetails(catMenu)));
                },
                child: Card(
                  elevation: 3.0,
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Hero(
                          tag: catMenu.idMenu,
                          child: FadeInImage.assetNetwork(
                            placeholder: './img/loading_icon.gif',
                            image: ApiUrl.baseUrl + catMenu.imageMenu,
                            fit: BoxFit.cover,
                            height: 150.0,
                            width: MediaQuery.of(context).size.width,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8.0, horizontal: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('${catMenu.nameMenu}'),
                            Text(
                                'Rp. ' + oCcy.format(int.parse(catMenu.price))),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      );
    }
  }
}
