import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:ketixclone/api.dart';
import 'package:ketixclone/models/buying.dart';
import 'package:http/http.dart' as http;

class ParaPembeli extends StatefulWidget {
  _ParaPembeliState createState() => _ParaPembeliState();
}

class _ParaPembeliState extends State<ParaPembeli> {
  final pembeli = List<Pembeli>();
  var isLoading = false;

  _fetchDataPembeli() async {
    pembeli.clear();
    setState(() {
      isLoading = true;
    });

    final responses = await http.get(ApiUrl.baseUrl + ApiUrl.paraPembeli);
    final data = jsonDecode(responses.body);
    print('$data');
    data.forEach((api) {
      final beli = new Pembeli(
          api['ID'],
          api['Name'],
          api['Alamat'],
          api['Kota'],
          api['Provinsi'],
          api['Number_of_people'],
          api['Date_n_Time'],
          api['Phone_number'],
          api['Order_list'],
          api['Status'],
          api['Comment'],
          api['Email']);
      pembeli.add(beli);
    });
    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    _fetchDataPembeli();
    isLoading = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        leading: FlatButton(
          child: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          'Status Pembeli',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: _buildPembeliBrownies(),
    );
  }

  Widget _buildPembeliBrownies() {
    if (isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return ListView.builder(
        itemCount: pembeli.length,
        itemBuilder: (context, index) {
          final belis = pembeli[index];
          return GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DetailPembeli(belis)));
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Text(
                        belis.nama,
                        style: TextStyle(
                            color: Colors.green,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Text(
                        belis.alamat,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Text(
                        belis.telpon,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      );
    }
  }
}

class DetailPembeli extends StatefulWidget {
  final Pembeli models;
  DetailPembeli(this.models);
  _DetailPembeliState createState() => _DetailPembeliState();
}

class _DetailPembeliState extends State<DetailPembeli> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text('Detil Pembeli'),
        centerTitle: true,
        leading: FlatButton(
          child: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Row(
              children: <Widget>[
                Text('Nama : '),
                Text(
                  widget.models.nama,
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Divider(),
            Row(
              children: <Widget>[
                Text('Alamat : '),
                Text(
                  widget.models.alamat,
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Divider(),
            Row(
              children: <Widget>[
                Text('Kota : '),
                Text(
                  widget.models.kota,
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Divider(),
            Row(
              children: <Widget>[
                Text('provinsi : '),
                Text(
                  widget.models.provinsi,
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Divider(),
            Row(
              children: <Widget>[
                Text('Pengiriman : '),
                Text(
                  widget.models.kurir,
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Divider(),
            Row(
              children: <Widget>[
                Text('Kapan Kirim : '),
                Text(
                  widget.models.kapanKirim,
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Divider(),
            Row(
              children: <Widget>[
                Text('No. Telpon : '),
                Text(
                  widget.models.telpon,
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Divider(),
            Row(
              children: <Widget>[
                Text('Pesanan : '),
                SizedBox(
                  width: 5.0,
                ),
                Text(
                  widget.models.pesanan,
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Divider(),
            Row(
              children: <Widget>[
                Text('Status : '),
                Text(
                  widget.models.status,
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Divider(),
            Row(
              children: <Widget>[
                Text('Pesan : '),
                Text(
                  widget.models.komen,
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Divider(),
            Row(
              children: <Widget>[
                Text('Email : '),
                Text(
                  widget.models.email,
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Divider(),
          ],
        ),
      ),
    );
  }
}
