import 'package:flutter/material.dart';

class NewsOffline extends StatefulWidget {
  _NewsOfflineState createState() => _NewsOfflineState();
}

class _NewsOfflineState extends State<NewsOffline> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        Container(
          height: 100.0,
          width: MediaQuery.of(context).size.width,
          child: Center(
            child: Text(
              'Produk tidak ditemukan',
              style: TextStyle(
                  color: Colors.brown,
                  fontSize: 16.0,
                  fontWeight: FontWeight.w300),
            ),
          ),
        ),
      ],
    );
  }
}
