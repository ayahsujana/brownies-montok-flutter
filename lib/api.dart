class ApiUrl {
  static String baseUrl = 'http://ayahsujana.com/brownies/';
  static String accessKey = '12345';
  static String categoryItem = 'api/get-all-category-data.php?accesskey=';
  static String detailItem =
      'api/get-menu-detail.php?accesskey=$accessKey&menu_id=';
  static String menuItem =
      'api/get-menu-data-by-category-id.php?accesskey=$accessKey&category_id=';
  static String sendTo = 'api/add-reservation.php';
  static String hotnews = 'api/get-all-menu-data.php?accesskey=';

  static String addTmpProduk = 'api/add-tmpproduk-menu.php';
  static String deleteTmpProduk = 'api/delete-By-Item-Produk-brownies-montok.php';
  static String tmpTransaksiTotal = 'api/get-tmp-transaksi-brownies-montok.php';
  static String updateQtyTransaksi = 'api/update-Tmp-Produk-Brownies-Montok.php';
  static String hapusSemuaTransaksi = 'api/delete-Tmp-Produk-brownies-montok.php';
  static String sumTmpTransaksi = 'api/get-Sum-Total-Tmp-Brownies-Montok.php?username=';
  static String allTmpTransaksi = 'api/get-tmp-transaksi-brownies-montok.php?username=';

  static String finish = 'api/addTransaksiBrowniesMontok.php';
  static String invTransaksi= 'api/getTransaksiBrowniesMontok.php?idCustomer=';
  static String detilTransaksi = 'api/getDetailTransaksiBrowniesMontok.php?idTransaksi=';
  static String sumDetailTransaksi = 'api/getSumDetailTransaksiBrowniesMontok.php?idTransaksi=';

  static String paraPembeli = 'api/getReservationBrowniesMontok.php?accesskey=$accessKey';

  static String tax = 'api/get-tax-and-currency.php?accesskey=$accessKey';

  static String search = '+&keyword=';

  static String login =
      'http://ayahsujana.com/brownies/api/login-brownies-montok.php';
  static String register =
      'http://ayahsujana.com/brownies/api/register-brownies-montok.php';
}
